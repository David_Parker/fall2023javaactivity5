package polymorphism;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookTest 
{
    @Test
    public void getMethods_returnsTrue()
    {
        Book b = new Book("Hello", "David Parker");
        assertEquals("Expected book title", "Hello", b.getTitle());
        assertEquals("Expected book author", "David Parker", b.getAuthor());
    }

}