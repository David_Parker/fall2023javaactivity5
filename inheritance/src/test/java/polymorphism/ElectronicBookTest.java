package polymorphism;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ElectronicBookTest {
    
    @Test
    public void getMethods_returnTrue() {
        ElectronicBook eBook = new ElectronicBook("Hello", "David Parker", 15000);
        assertEquals("Expected the number of bytes", 15000, eBook.getNumberBytes());
    }
}
